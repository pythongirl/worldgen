extern crate num;
extern crate rand;

const SHADE_CHARS: [char; 4] = ['\u{2593}', '\u{2592}', '\u{2591}', ' '];

const BLOCK_CHARS: [char; 8] = [
    '\u{2581}', '\u{2582}', '\u{2583}', '\u{2584}', '\u{2585}', '\u{2586}', '\u{2587}', '\u{2588}',
];

use num::Complex;
use rand::prelude::*;
use std::fmt;

const PI: f64 = std::f64::consts::PI;
const TAU: f64 = PI * 2.0;

fn random_angle() -> f64 {
    random::<f64>() * TAU
}

fn random_dist(mean: f64, std_dev: f64) -> f64 {
    let distrobution = rand::distributions::Normal::new(mean, std_dev);

    distrobution.sample(&mut thread_rng())
}

fn check_point(point: &Point, size: f64) -> bool {
    point.re < size && point.re > 0.0 && point.im < size && point.im > 0.0
}

type Point = Complex<f64>;

#[derive(Clone, Debug)]
struct GenRegion {
    shelf_level: i32, // -6 being the base ocean, 0 being shoreline, anything greater being land
}

impl GenRegion {
    fn increment(&mut self) {
        self.shelf_level += 1;
    }

    fn default() -> GenRegion {
        GenRegion { shelf_level: -6 }
    }
}

#[derive(Debug)]
struct GenWorld {
    size: usize,
    regions: Vec<Vec<GenRegion>>,
}

impl GenWorld {
    fn new(size: usize) -> GenWorld {
        GenWorld {
            size,
            regions: vec![vec![GenRegion::default(); size]; size],
        }
    }

    fn get_region_index(&self, point: &Point) -> (usize, usize) {
        let x_coord = point.re;
        let y_coord = -(point.im) as i32 + (self.size as i32) - 1;

        (x_coord as usize, y_coord as usize)
    }

    fn get_region_mut(&mut self, point: &Point) -> Option<&mut GenRegion> {
        let (x_index, y_index) = self.get_region_index(point);
        self.regions.get_mut(y_index)?.get_mut(x_index)
    }

    #[allow(dead_code)] // TODO: Remove this once I use it.
    fn get_region(&self, point: &Point) -> Option<&GenRegion> {
        let (x_index, y_index) = self.get_region_index(point);
        self.regions.get(y_index)?.get(x_index)
    }

    fn increment_region(&mut self, point: &Point) {
        if check_point(&point, self.size as f64) {
            self.get_region_mut(point).unwrap().increment()
        }
    }


    fn generate(&mut self) {
        let size = self.size as f64;

        let map_center = Point::new(size / 2.0, size / 2.0);

        let hotspot_angle = random_angle();
        let hotspot_start = map_center
            + Point::from_polar(&(random_dist(size * 0.25, size * 0.025)), &hotspot_angle);

        let time_divisions = size * 0.3;

        let hotspot_velocity = Point::from_polar(
            &(random_dist(size / 2.0 / time_divisions, size / 20.0 / time_divisions)),
            &random_dist(hotspot_angle + PI, PI / 8.0),
        );

        let mut current_hotspot = hotspot_start;
        for _ in 0..(time_divisions as i32) {
            current_hotspot += hotspot_velocity;

            let distrobution = rand::distributions::Normal::new(0.0, size / 3.0);

            let temperature = random_dist(78.0, 3.0);

            (0..(temperature as i32))
                .map(|_| current_hotspot + Point::from_polar(&distrobution.sample(&mut thread_rng()), &random_angle()))
                .filter(|eruption_point| check_point(eruption_point, size))
                .for_each(|eruption_point| {
                    self.increment_region(&eruption_point);

                    let eruption_point_level = self.get_region(&eruption_point).unwrap().shelf_level;

                    (0..4)
                        .map(|a| eruption_point + Point::from_polar(&1.0, &(f64::from(a) * TAU / 4.0)))
                        .filter(|point| check_point(point, size))
                        .filter(|lava_flow_point| {
                            let dest_level = self.get_region(lava_flow_point).unwrap().shelf_level;
                            dest_level < eruption_point_level
                        })
                        .for_each(|lava_flow_point| {
                            self.increment_region(&lava_flow_point);
                        });
                });

            // for _ in 0..(temperature as i32) {
            //     let eruption_point = current_hotspot
            //         + Point::from_polar(&distrobution.sample(&mut thread_rng()), &random_angle());

            //     if check_point(&eruption_point, size) {

            //         self.increment_region(eruption_point);

            //         let eruption_point_level = self.get_region(eruption_point).unwrap().shelf_level;

            //         (0..4)
            //             .map(|a| eruption_point + Point::from_polar(&1.0, &(f64::from(a) * TAU / 4.0)))
            //             .filter(|point| check_point(point, size))
            //             .for_each(|lava_flow_point| {
            //                 let destination_level = self.get_region(lava_flow_point).unwrap().shelf_level;

            //                 if destination_level < eruption_point_level {
            //                     self.increment_region(lava_flow_point);
            //                 }
            //             });
            //     }
            // }
        }
    }
}


impl fmt::Display for GenWorld {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for row in self.regions.iter() {
            for region in row.iter() {
                let level = region.shelf_level;
                if level <= -5 {
                    write!(f, "\u{2588}\u{2588}")?;
                } else if level <= -1 {
                    write!(f, "{0}{0}", SHADE_CHARS[(level + 4) as usize])?;
                } else if level <= 7 {
                    write!(f, "{0}{0}", BLOCK_CHARS[level as usize])?
                } else {
                    write!(f, "\u{26f0} ")?;
                }
            }
            writeln!(f)?;
        }

        Ok(())
    }
}

fn main() {
    let mut world = GenWorld::new(32);
    world.generate();

    println!("World:\n{}", world);
}
